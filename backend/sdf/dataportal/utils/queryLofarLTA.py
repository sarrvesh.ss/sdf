def queryLofarLTA(project, private_data=False):
	"""
	Query the LOFAR LTA Catalogue
	params:
		project: The project to query, e.g. LC2_035 has public data
		private_data: Query for private data of the project if set True
	"""
	from datetime import datetime
	from awlofar.database.Context import context
	from awlofar.main.aweimports import CorrelatedDataProduct, FileObject, Observation
	from awlofar.toolbox.LtaStager import LtaStager, LtaStagerError
	from awlofar.config.Environment import Env

	# Should the found files be staged?
	do_stage = False
	# The class of data to query
	cls = CorrelatedDataProduct

	query_observations = Observation.select_all().project_only(project)
	dataproduct_query =[]
	for observation in query_observations:
		dataproduct_query.append(observation.as_dict())
	
	return(dataproduct_query)
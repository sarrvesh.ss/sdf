from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import LofarLTASerializer
from .utils.queryLofarLTA import queryLofarLTA
from .models import LofarLTAObservation


@api_view(['GET', 'PUT'])
def observation_list(request):
	if request.method == 'GET':
		queryset = LofarLTAObservation.objects.all()
		serializer_class = LofarLTASerializer
		filterset_fields = ['createDate', 'dataProductType', 'duration', 'dataProductIdentifier', 'filename']
		return Response(serializer_class.data)

	elif request.method == 'PUT':
		query = request.data
		project = query['params']['project']
		query_results = queryLofarLTA(project=project)
		#serializer_class = LofarLTASerializer(query_results, many=True)

		return Response(query_results)
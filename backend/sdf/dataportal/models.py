from django.db import models

# Create your models here.

class LofarLTAObservation(models.Model):
	createDate = models.DateField()
	dataProductType = models.CharField(max_length=50)
	duration = models.FloatField()
	dataProductIdentifier = models.CharField(max_length=50)
	filename = models.CharField(max_length=50)

	def __str__(self):
 		return self.dataProductIdentifier

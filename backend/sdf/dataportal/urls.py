from django.urls import path, include
from .views import observation_list

urlpatterns = [
    path('api/observations/', observation_list, name='observations')
]
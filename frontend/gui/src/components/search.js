import React from 'react';
import axios from 'axios';
import { InputGroup, Input, Button } from 'reactstrap';
import Observations from './observations';

export default class Search extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
      searchString: '',
      observations: []
    };
	}

	handleChange = (event) => {
    this.setState({
      searchString: event.target.value
    });
  }

  handleSubmit = (event) => {
    // need to parse searchString if more complicated query is allowed
    axios.put('http://127.0.0.1:8000/api/observations/',{
      params: {
        project: this.state.searchString
      }
    })
    .then( res => {
      console.log(res.data);
      this.setState({
        observations: res.data
      });
    })
    .catch( error => {
      console.log(error);
    });
  }

	render() {
		return (
			<div>
				<InputGroup>
          <Input
            type="search"
            placeholder="e.g. LC9_017"
            value={this.state.searchString}
            onChange={this.handleChange}
          />
          <Button onClick={this.handleSubmit}>Search</Button>
        </InputGroup>
        <Observations observations={this.state.observations}/>
	    </div>
		);
  }
}
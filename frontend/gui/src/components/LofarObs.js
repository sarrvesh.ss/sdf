import React from 'react';
import { Button, Card, CardBody, CardTitle, CardText } from 'reactstrap';

export default class LofarObs extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			viewDetail: false,
		};
	}

	displayDetail = () => {
		this.setState({
			viewDetail: !this.state.viewDetail
		});
	}

	render() {
		const observation = this.props.observation
		return (
			<div>
				<Button onClick={this.displayDetail} color="link">
					{observation["Observation.observationId"]}
				</Button>
				{this.state.viewDetail ? (
					<Card>
						<CardBody>
							<CardTitle>
								Target: {observation["Observation.observationDescription"]}
							</CardTitle>
							<CardText>
								{observation["Observation.projectInformation.projectDescription"]}
							</CardText>
						</CardBody>
					</Card>
				) : null}
			</div>
		);
	}
}
import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import LofarObs from './LofarObs';

export default function Observations(props) {

	const observations = props.observations;
	return (
		<ListGroup>
			{observations.map(
				(observation) => 
					<ListGroupItem key={observation["Observation.observationId"]}>
						<LofarObs observation={observation} />
					</ListGroupItem>
			)}
    </ListGroup>
	);
} 

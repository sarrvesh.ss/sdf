# ASTRON Science Delivery Framework



## For Developers

### Deployment

Prerequisition:
* git
* python3
* npm

```bash
# setup home directory as install directory
installdir=$HOME
# Clone the repository into home directory
cd $installdir
git clone git@gitlab.com:astron-sdc/sdf.git
cd sdf/backend
# create virtual environment
python3 -m venv env
# activate virtual environment
source env/bin/activate
pip install -r requirements.txt
# Install LOFAR LTA Client as described on the LOFAR wiki 
# See https://www.astron.nl/lofarwiki/doku.php?id=lta:client_installation
# Make sure you update your ${HOME}/.awe/Environment.cfg file
# The LTA Client installation step will create a new directory called
# instantclient_11_2 under lib/ . Include this directory under LD_LIBRARY_PATH
# Also, make sure to install the Oracle Client library (on Ubuntu, it is libaio1). 
cd sdf
python manage.py makemigrations
python manage.py migrate --run-syncdb

python manage.py runserver

cd $installdir
cd sdf/frontend/gui
npm install --save-dev
npm start
```

